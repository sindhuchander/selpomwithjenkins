package com.framework.cases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_loginLogout extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login to Leaftaps";
		testNodes = "Leads";
		author = "SPC";
		category = "smoke";
		dataSheetName = "TC001";
		
	}
	
	@Test
	// if excel present - give this : @Test(dataProvide="fetchData")
	// this in turn picks up the fetch data data provider from 
	public void login()
	{
		new LoginPage()
		.enterUsername("DemoCSR")
		.enterPassword("crmsfa")
		.clickLogin()
		.clickCRMSFA() // new HomePage() need not be called, since per our design, clickLogin returns HomePage
		.clickMyLeads()
		.clickCreateLeads()
		.enterFirstName("Titan")
		.enterLastName("Raga")
		.enterCompanyName("TestLeaf")
		.clickCreateLeadButton();
	}
	

}
