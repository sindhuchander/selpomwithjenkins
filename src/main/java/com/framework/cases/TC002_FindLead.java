/*package com.framework.cases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_FindLead extends ProjectMethods {
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC002_FindLead";
		testDescription = "Find Lead Module";
		testNodes = "Leads";
		author = "SPC";
		category = "smoke";
		dataSheetName = "TC002";
		
	}
	
	@Test
	// if excel present - give this : @Test(dataProvide="fetchData")
	// this in turn picks up the fetch data data provider from 
	public void login()
	{
		new LoginPage()
		.enterUsername("DemoCSR")
		.enterPassword("crmsfa")
		.clickLogin()
		.clickCRMSFA() // new HomePage() need not be called, since per our design, clickLogin returns HomePage
		.clickMyLeads()
		.clickFindLeads()
		.enterFirstName("Titan")
		.clickFindLeadButton();

		.enterLastName("Raga")
		.enterCompanyName("TestLeaf")
		.clickCreateLeadButton();
	}

}
*/