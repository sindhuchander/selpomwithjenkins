package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods {

	public FindLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//input[@name='firstName'])[3]")  WebElement eleFirstName;
	public FindLeadPage enterFirstName(String data)
	{
		clearAndType(eleFirstName, data);
		return this;
	}
	
	@FindBy(how= How.XPATH, using="//button[text()=\"Find Leads\"]") WebElement eleFindButton;
	public FindLeadPage clickFindLeadButton()
	{
		click(eleFindButton);
		return this;
	}
	
/*	

locateElement("xpath", "//button[text()=\"Find Leads\"]").click();
		// driver.findElementByXPath("//button[text()=\"Find Leads\"]").click();

WebElement eleFindButton = locateElement("xpath", "(//[@text() = 'Find Leads'])[2]");
click(eleFindButton);	*/
	
}
