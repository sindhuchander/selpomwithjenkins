package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_firstName")  WebElement eleFirstName;
	public CreateLead enterFirstName(String data)
	{
		clearAndType(eleFirstName, data);
		return this;
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_lastName")  WebElement eleLastName;
	public CreateLead enterLastName(String data)
	{
		clearAndType(eleLastName, data);
		return this;
	}
	
	@FindBy(how = How.ID, using = "createLeadForm_companyName")  WebElement eleCompanyName;
	public CreateLead enterCompanyName(String data)
	{
		//clearAndType(eleCompanyName, data);
		clearAndType(eleCompanyName, data);
		return this;
	}
	
	@FindBy(how = How.NAME, using = "submitButton") WebElement eleCreateLead;
	public ViewLead clickCreateLeadButton()
	{
		//		driver.findElementByName("submitButton").click();
		click(eleCreateLead);
		return new ViewLead();
	}
		
}
	
