package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeads extends ProjectMethods {
	
	public MyLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Create Lead")  WebElement eleCreateLead;
	public CreateLead clickCreateLeads()
	{
		click(eleCreateLead);
		return new CreateLead();
	}
	
	@FindBy(how = How.LINK_TEXT, using="Find Leads") WebElement eleFindLead;
	public FindLeadPage clickFindLeads()
	{
		click(eleFindLead);
		return new FindLeadPage();
	}
}
