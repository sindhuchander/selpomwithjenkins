package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyHomePage extends ProjectMethods {
	
	public MyHomePage() // Constructor using Page Factory concept
	{
		PageFactory.initElements(driver, this);
		// Page factor is a class which helps us to find element in a web page
	}
	
	@FindBy(how = How.LINK_TEXT,using="Leads") WebElement eleLeads;
	public MyLeads clickMyLeads()
	{
		click(eleLeads);
		return new MyLeads();
	}
		
}
